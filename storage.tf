resource "local_file" "nfs-condo" {
  filename             = "${path.module}/${var.cluster_name}-init/nfs-condo.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content              = <<-EOT
    storageClass:
      name: nfs-condo
      archiveOnDelete: false
      reclaimPolicy: Retain
      defaultClass: true

    nfs:
      server: radiant-nfs.ncsa.illinois.edu
      path: /radiant/projects/${data.openstack_identity_auth_scope_v3.scope.project_name}/${var.cluster_name}
  EOT
}
