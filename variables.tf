variable "cluster_name" {
  type        = string
  description = "Desired name of new cluster"
}

# ----------------------------------------------------------------------
# RANCHER
# ----------------------------------------------------------------------

variable "rancher2_api_url" {
  type        = string
  description = "URL where rancher runs"
  default     = "https://gonzo-rancher.ncsa.illinois.edu/v3"
}

variable "rancher2_access_key" {
  type        = string
  description = "Access key for rancher"
}

variable "rancher2_secret_key" {
  type        = string
  description = "Secret for rancher"
}

# ----------------------------------------------------------------------
# RANCHER KUBERNETES
# ----------------------------------------------------------------------

variable "monitoring_enabled" {
  type        = bool
  description = "Enable monitoring in rancher"
  default     = true
}

variable "longhorn_enabled" {
  type        = bool
  description = "Enable longhorn storage"
  default     = true
}

variable "longhorn_replica" {
  type        = string
  description = "Number of replicas"
  default     = 3
}

variable "cluster_description" {
  type        = string
  description = "Description of new cluster"
  default     = ""
}

# ----------------------------------------------------------------------
# OPENSTACK
# ----------------------------------------------------------------------

variable "openstack_username" {
  type        = string
  description = "OpenStack Username"
}

variable "openstack_password" {
  type        = string
  description = "OpenStack Password"
}

variable "openstack_projectid" {
  type        = string
  description = "OpenStack Project ID"
}

variable "openstack_url" {
  type        = string
  description = "OpenStack URL"
  default     = "https://radiant-old.ncsa.illinois.edu:5000/v3/"
}

# ----------------------------------------------------------------------
# OPENSTACK KUBERNETES
# ----------------------------------------------------------------------

variable "os" {
  type        = string
  description = "Base image to use for the OS"
  default     = "CentOS-7-GenericCloud-Latest"
}

variable "controlplane_count" {
  type        = string
  description = "Desired quantity of control-plane nodes"
  default     = 1
}

variable "controlplane_flavor" {
  type        = string
  description = "Desired flavor of control-plane nodes"
  default     = "m1.medium"
}

variable "controlplane_disksize" {
  type        = string
  description = "Desired disksize of control-plane nodes"
  default     = 40
}

variable "worker_count" {
  type        = string
  description = "Desired quantity of worker nodes"
  default     = 1
}

variable "worker_flavor" {
  type        = string
  description = "Desired flavor of worker nodes"
  default     = "m1.large"
}

variable "worker_disksize" {
  type        = string
  description = "Desired disksize of worker nodes"
  default     = 40
}

variable "metallb_floating_ip" {
  type        = string
  description = "Number of floating IP addresses available for metallb"
  default     = 2
}

# ----------------------------------------------------------------------
# INGRESS
# working:
# - traefik1
# - traefik2
# - nginx
# - none
# work in progress
# - nginxinc
# ----------------------------------------------------------------------

variable "ingress_controller" {
  type        = string
  description = "Desired ingress controller (traefik1, traefik2, nginxinc, nginx, none)"
  default     = "traefik2"
}

# ----------------------------------------------------------------------
# TRAEFIK
# ----------------------------------------------------------------------

variable "traefik_dashboard" {
  type        = bool
  description = "Should dashboard ingress rule be added as /traefik"
  default     = true
}

variable "traefik_server" {
  type        = string
  description = "Desired hostname to be used for cluster, xip.io will use ip address"
  default     = ""
}

variable "traefik_access_log" {
  type        = bool
  description = "Should traefik enable access logs"
  default     = false
}

variable "traefik_use_certmanager" {
  type        = bool
  description = "Should traefik v2 use cert manager"
  default     = false
}

# ----------------------------------------------------------------------
# LETS ENCRYPT
# ----------------------------------------------------------------------

variable "acme_staging" {
  type        = bool
  description = "Use the staging server"
  default     = false
}

variable "acme_email" {
  type        = string
  description = "Use the following email for cert messages"
  default     = "example@lists.example.com"
}
