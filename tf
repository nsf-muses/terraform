#!/bin/bash

SPACE=$(grep '"address"' .terraform/terraform.tfstate | sed 's#.*/\(.*\)",#\1#')
if [ $# != 0 ]; then
  sed -i~ "s/${SPACE}/$1/g" .terraform/terraform.tfstate
  SPACE=$1
  shift
fi

if [ $# == 0 ]; then
  CMD="apply"
else
  CMD=$1
  shift
fi

if [ -e secrets.tfvars ]; then
  SECRETS="-var-file=secrets.tfvars"
fi

${DEBUG} terraform $CMD -var-file=${SPACE}.tfvars ${SECRETS} -var cluster_name=${SPACE} $@
