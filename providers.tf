terraform {
  backend "http" {
  }
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
    }
    rancher2 = {
      source = "rancher/rancher2"
    }
    local = {
      source = "hashicorp/local"
    }
  }
}

provider "openstack" {
  auth_url            = var.openstack_url
  region              = "RegionOne"
  user_name           = var.openstack_username
  password            = var.openstack_password
  user_domain_name    = "ncsa"
  tenant_id           = var.openstack_projectid
}

provider "rancher2" {
  api_url    = var.rancher2_api_url
  access_key = var.rancher2_access_key
  secret_key = var.rancher2_secret_key
}
