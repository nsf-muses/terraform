# ----------------------------------------------------------------------
# cluster definition
# ----------------------------------------------------------------------
resource "rancher2_cluster" "kube" {
  name        = var.cluster_name
  description = var.cluster_description
  driver      = "rancherKubernetesEngine"

  cluster_auth_endpoint {
    enabled = false
  }

  rke_config {
    network {
      plugin = "weave"
    }
    ingress {
      provider = "none"
    }
    upgrade_strategy {
      drain                        = true
      drain_input {
        delete_local_data          = true
        ignore_daemon_sets         = true
        timeout                    = 120
      }
      max_unavailable_controlplane = 1
      max_unavailable_worker       = 1
    }
  }
}

# Create a new rancher2 Cluster Sync for foo-custom cluster
resource "rancher2_cluster_sync" "kube" {
  depends_on      = [ openstack_compute_instance_v2.controlplane[0], openstack_compute_instance_v2.worker[0] ]
  cluster_id      = rancher2_cluster.kube.id
}

resource "local_file" "kubeconfig" {
  content         = rancher2_cluster_sync.kube.kube_config
  filename        = pathexpand("~/.kube/${var.cluster_name}.kubeconfig")
  file_permission = "0600"
}

# ----------------------------------------------------------------------
# longhorn storage
# ----------------------------------------------------------------------
resource "rancher2_app_v2" "longhorn-system" {
  count         = var.longhorn_enabled ? 1 : 0
  cluster_id    = rancher2_cluster_sync.kube.cluster_id
  name          = "longhorn"
  namespace     = "longhorn-system"
  repo_name     = "rancher-charts"
  chart_name    = "longhorn"
  project_id    = rancher2_cluster_sync.kube.system_project_id
  values        = <<EOF
defaultSettings:
  backupTarget: nfs://radiant-nfs.ncsa.illinois.edu:/radiant/projects/${data.openstack_identity_auth_scope_v3.scope.project_name}/${var.cluster_name}/backup
  defaultReplicaCount: ${var.longhorn_replica}
persistence:
  defaultClass: false
  defaultClassReplicaCount: ${var.longhorn_replica}
EOF
   lifecycle {
     ignore_changes = [
       values
     ]
   }
}

# ----------------------------------------------------------------------
# monitoring
# ----------------------------------------------------------------------
resource "rancher2_app_v2" "monitor" {
  count         = var.monitoring_enabled ? 1 : 0
  cluster_id    = rancher2_cluster_sync.kube.cluster_id
  name          = "rancher-monitoring"
  namespace     = "cattle-monitoring-system"
  repo_name     = "rancher-charts"
  chart_name    = "rancher-monitoring"
  project_id    = rancher2_cluster_sync.kube.system_project_id
//  values        = <<EOF
//prometheus:
//  resources:
//    core:
//      limits:
//        cpu: "4000m"
//        memory: "6144Mi"
//EOF
  lifecycle {
    ignore_changes = [
      values
    ]
  }
}
