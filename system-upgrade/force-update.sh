#!/bin/bash

kubectl -n system-upgrade delete job os-upgrade
kubectl -n system-upgrade create job --from cronjob/os-upgrade os-upgrade
