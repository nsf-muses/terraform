#!/bin/bash

cd $(dirname $0)

# mark control-plane nodes
kubectl label node --overwrite --selector='node-role.kubernetes.io/controlplane' upgrade.cattle.io/controller=system-upgrade-controller

# load system-upgrade
kubectl apply -f system-upgrade-controller.yaml

# wait for pod to run (and plan type to exist)
while [ "$(kubectl -n system-upgrade get pods | awk '/^system-upgrade/ { print $3 }')" != "Running" ]; do
  sleep 1
done

# load plans
kubectl apply -f bionic.yaml -f centos7.yaml

# load cronjob
kubectl apply -f cronjob.yaml

# run cronjob
kubectl -n system-upgrade delete job os-upgrade
kubectl -n system-upgrade create job --from cronjob/os-upgrade os-upgrade
