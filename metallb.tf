# generate floating ips that can be used by the services with loadbalancer
# this is used for example for traefik

# create a port that will be used with the floating ip, this will be associated
# with all of the VMs.
resource "openstack_networking_port_v2" "metallb_ip" {
  count       = var.metallb_floating_ip
  depends_on  = [ openstack_networking_subnet_v2.cluster_subnet ]
  name        = "${var.cluster_name}-metallb-ip-${count.index}"
  network_id  = openstack_networking_network_v2.cluster_net.id
}

# create floating ip that is associated with a fixed ip
resource "openstack_networking_floatingip_v2" "metallb_ip" {
  count   = var.metallb_floating_ip
  pool    = data.openstack_networking_network_v2.ext_net.name
  port_id = element(openstack_networking_port_v2.metallb_ip.*.id, count.index)
}

# create the metallb configuration file
resource "local_file" "metallb-config" {
  filename             = "${path.module}/${var.cluster_name}-init/metallb-config.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    apiVersion: v1
    kind: ConfigMap
    metadata:
      namespace: metallb-system
      name: config
    data:
      config: |
        address-pools:
        - name: default
          protocol: layer2
          addresses:
    %{ for ip in flatten(openstack_networking_port_v2.metallb_ip.*.all_fixed_ips) ~}
      - ${ip}/32
    %{ endfor ~}
  EOT
}
