# Ref: https://www.terraform.io/docs/configuration/locals.html
locals {
  ingress_private_ip    = element(flatten(openstack_networking_port_v2.metallb_ip.*.all_fixed_ips), 0)
  ingress_public_ip     = element(openstack_networking_floatingip_v2.metallb_ip.*.address, 0)
  ingress_installed     = var.ingress_controller == "traefik1" || var.ingress_controller == "traefik2" || var.ingress_controller == "nginx" || var.ingress_controller == "nginxinc"
  certmanager_installed = (var.ingress_controller == "traefik2" && var.traefik_use_certmanager) || var.ingress_controller == "nginx" || var.ingress_controller == "nginxinc"
}

# ----------------------------------------------------------------------
# TRAEFIK v1 INGRESS
# ----------------------------------------------------------------------
resource "local_file" "traefik1_values" {
  count                = var.ingress_controller == "traefik1" ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/traefik1.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    loadBalancerIP: ${ local.ingress_private_ip }
    externalIP: ${ local.ingress_public_ip }
    externalTrafficPolicy: Local
    rbac:
      enabled: true
    dashboard:
      enabled: true
      domain: traefik.${ local.ingress_public_ip }.nip.io
      ingress:
        annotations:
    acme:
      enabled: true
      challengeType: http-01
      email: ${ var.acme_email }
      staging: ${ tostring(var.acme_staging) }
      logging: true
      persistence:
        enabled: true
%{~ if var.longhorn_enabled }
        storageClass: longhorn
%{~ endif }
        accessMode: ReadWriteMany
    ssl:
      tlsMinVersion: VersionTLS12
      cipherSuites:
        - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
        - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
      enabled: true
      enforced: true
      insecureSkipVerify: true
    kubernetes:
      ingressEndpoint:
        ip: ${ local.ingress_public_ip }
%{~ if var.traefik_access_log }
    accessLogs:
      enabled: true
      filePath: "/dev/stdout"
      format: common
%{~ endif }
EOT
}

# ----------------------------------------------------------------------
# TRAEFIK v2 INGRESS
# ----------------------------------------------------------------------
resource "local_file" "traefik2_values" {
  count                = var.ingress_controller == "traefik2" ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/traefik2.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    service:
      externalIP:
        - ${ local.ingress_public_ip }
      spec:
        externalTrafficPolicy: Local
        loadBalancerIP: ${ local.ingress_private_ip }
    ports:
      web:
        redirectTo: websecure
      websecure:
        tls:
          enabled: true
          %{~ if (! local.certmanager_installed) ~}
          certResolver: letsencrypt
          %{~ endif ~}
    #providers:
    #  kubernetesIngress:
    #    publishedService:
    #      enabled: true
    additionalArguments:
      - --providers.kubernetesingress.ingressendpoint.ip=${ local.ingress_public_ip }
      %{~ if (! local.certmanager_installed) ~}
      %{~ if (var.acme_staging) ~}
      - --certificatesresolvers.letsencrypt.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory
      %{~ else ~}
      - --certificatesresolvers.letsencrypt.acme.caserver=https://acme-v02.api.letsencrypt.org/directory
      %{~ endif ~}
      - --certificatesresolvers.letsencrypt.acme.email=${var.acme_email}
      - --certificatesresolvers.letsencrypt.acme.storage=/data/acme.json
      - --certificatesresolvers.letsencrypt.acme.tlschallenge=true
    %{~ endif ~}
    %{~ if (! local.certmanager_installed) ~}
    persistence:
      enabled: true
    %{~ endif ~}
    ingressRoute:
      dashboard:
        enabled: false
    tlsOptions:
      default:
        minVersion: VersionTLS12
        cipherSuites:
          - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
          - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
          - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
          - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
          - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
          - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
EOT
}

resource "local_file" "traefik2_dashboard_values" {
  count                = var.ingress_controller == "traefik2" ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/traefik2_dashboard.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    apiVersion: traefik.containo.us/v1alpha1
    kind: IngressRoute
    metadata:
      name: traefik-dashboard
      namespace: traefik
    spec:
      entryPoints:
        - websecure
      routes:
        #- match: Host(`traefik.${ local.ingress_public_ip }.nip.io`) && (PathPrefix(`/dashboard`) || PathPrefix(`/api`))
        - match: Host(`traefik.${ local.ingress_public_ip }.nip.io`)
          kind: Rule
          services:
            - name: api@internal
              kind: TraefikService
    %{~ if (local.certmanager_installed) ~}
      tls:
        secretName: traefik.${ local.ingress_public_ip }.nip.io
    %{~ endif ~}
    %{~ if (local.certmanager_installed) ~}
    ---
    apiVersion: cert-manager.io/v1
    kind: Certificate
    metadata:
      name: traefik.${ local.ingress_public_ip }.nip.io
      namespace: traefik
    spec:
      dnsNames:
        - traefik.${ local.ingress_public_ip }.nip.io
      secretName: traefik.${ local.ingress_public_ip }.nip.io
      issuerRef:
        name: letsencrypt-prod
        kind: ClusterIssuer
    %{~ endif ~}
EOT
}

# ----------------------------------------------------------------------
# NGINX KUBERNETES INGRESS
# ----------------------------------------------------------------------
resource "local_file" "nginx_values" {
  count                = var.ingress_controller == "nginx" ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/nginx.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    controller:
      service:
        externalTrafficPolicy: Local
        loadBalancerIP: ${ local.ingress_private_ip }
      publishService:
        enabled: false
      extraArgs:
        publish-status-address: ${ local.ingress_public_ip }
EOT
}

# ----------------------------------------------------------------------
# NGINX INC INGRESS
# ----------------------------------------------------------------------
resource "local_file" "nginxinc_values" {
  count                = var.ingress_controller == "nginxinc" ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/nginxinc.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    controller:
      setAsDefaultIngress: true
      service:
        externalTrafficPolicy: Local
        loadBalancerIP: ${ local.ingress_private_ip }
      config:
        entries:
          external-status-address: ${ local.ingress_public_ip }
EOT
}

# ----------------------------------------------------------------------
# CERT MANAGER
# ----------------------------------------------------------------------
resource "local_file" "certmanager_values" {
  count                = local.certmanager_installed ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/cert-manager.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    installCRDs: true
    ingressShim:
      %{~ if (var.acme_staging) ~}
      defaultIssuerName: letsencrypt-staging
      %{~ else ~}
      defaultIssuerName: letsencrypt-prod
      %{~ endif ~}
      defaultIssuerKind: ClusterIssuer
EOT
}

resource "local_file" "letsencrypt_values" {
  count                = local.certmanager_installed ? 1 : 0
  filename             = "${path.module}/${var.cluster_name}-init/letsencrypt.yaml"
  directory_permission = "0755"
  file_permission      = "0600"
  content         = <<-EOT
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-staging
    spec:
      acme:
        email: ${ var.acme_email }
        server: https://acme-staging-v02.api.letsencrypt.org/directory
        privateKeySecretRef:
          name: lets-encrypt
        solvers:
          - http01:
              ingress:
                class: ""
    ---
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-prod
    spec:
      acme:
        email: ${ var.acme_email }
        server: https://acme-v02.api.letsencrypt.org/directory
        privateKeySecretRef:
          name: lets-encrypt
        solvers:
          - http01:
              ingress:
                class: ""
EOT
}

# ----------------------------------------------------------------------
# README
# ----------------------------------------------------------------------
resource "local_file" "readme" {
  filename             = "${path.module}/${var.cluster_name}-init/README"
  directory_permission = "0755"
  file_permission      = "0644"
  content              = <<-EOT
# ----------------------------------------------------------------------
# OPENSTACK SERVERS
# ----------------------------------------------------------------------
openstack private key = ${ pathexpand("~/.ssh/${var.cluster_name}.pem") }

# control-plane nodes
%{~ for x in openstack_compute_instance_v2.controlplane.* }
ssh -i ${ pathexpand("~/.ssh/${var.cluster_name}.pem") } centos@${x.network[1].fixed_ip_v4}
%{~ endfor }

%{~ if var.metallb_floating_ip > 0 }
# ----------------------------------------------------------------------
# METAL LB (load balancer)
# ----------------------------------------------------------------------
%{~ for i in range( var.metallb_floating_ip) }
floating ip ${ i } = ${ element(openstack_networking_floatingip_v2.metallb_ip.*.address, i) } == ${ element(flatten(openstack_networking_port_v2.metallb_ip.*.all_fixed_ips), i) }
%{~ endfor }

%{~ endif }
%{~ if var.ingress_controller == "traefik1" }
# ----------------------------------------------------------------------
# TRAEFIK v1 (ingress controller)
# ----------------------------------------------------------------------
floating ip for traefik = ${ local.ingress_public_ip }
dashboard for traefik   = https://traefik.${ local.ingress_public_ip }.nip.io/

%{~ endif }
%{~ if var.ingress_controller == "traefik2" }
# ----------------------------------------------------------------------
# TRAEFIK v2 (ingress controller)
# ----------------------------------------------------------------------
floating ip for traefik = ${ local.ingress_public_ip }
dashboard for traefik   = https://traefik.${ local.ingress_public_ip }.nip.io/

%{~ endif }
%{~ if var.ingress_controller == "nginx" || var.ingress_controller == "nginxinc" }
# ----------------------------------------------------------------------
# NGINX (ingress controller)
# ----------------------------------------------------------------------
floating ip for nginx = ${ local.ingress_public_ip }

%{~ endif }
# ----------------------------------------------------------------------
# KUBERNETES
# ----------------------------------------------------------------------
kubeconfig = ${ pathexpand("~/.kube/${var.cluster_name}.kubeconfig") }
export KUBECONFIG="${ pathexpand("~/.kube/${var.cluster_name}.kubeconfig") }"
  EOT
}

# ----------------------------------------------------------------------
# INITIALIZE SCRIPT
# ----------------------------------------------------------------------
resource "local_file" "initialize" {
  filename             = "${path.module}/${var.cluster_name}-init/initialize.sh"
  directory_permission = "0755"
  file_permission      = "0755"
  content              = <<-EOT
#!/bin/bash

# change directory
cd $(dirname $0)

# switch to correct cluster config
export KUBECONFIG="${ pathexpand("~/.kube/${var.cluster_name}.kubeconfig") }"

# install helm repos and update
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
%{~ if var.ingress_controller != "traefik1" }
helm repo add jetstack https://charts.jetstack.io
%{~ endif }
%{~ if var.ingress_controller == "nginx" }
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
%{~ endif }
%{~ if var.ingress_controller == "nginxinc" }
helm repo add nginx-stable https://helm.nginx.com/stable
%{~ endif }
%{~ if var.ingress_controller == "traefik2" }
helm repo add traefik https://helm.traefik.io/traefik
%{~ endif }
helm repo update

# make sure cluster folder exists in storage condo
ssh -o StrictHostKeyChecking=no -i ${var.cluster_name}.pem centos@${openstack_compute_instance_v2.controlplane[0].network[1].fixed_ip_v4} \
  'sudo mount radiant-nfs.ncsa.illinois.edu:/radiant/projects/${data.openstack_identity_auth_scope_v3.scope.project_name} /mnt && ' \
  'sudo mkdir -p /mnt/${var.cluster_name}/backup && ' \
  'sudo umount /mnt'

# install condo nfs
echo "# Install nfs storage client for condo"
helm upgrade --install --create-namespace --namespace nfs-condo nfs-condo nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --values nfs-condo.yaml
echo ""

# install metallb
echo "# Installing metallb"
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/namespace.yaml | grep -v unchanged
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml | grep -v unchanged
kubectl apply -n metallb-system -f metallb-config.yaml | grep -v unchanged
echo ""

# install secret for metallb
kubectl get secret -n metallb-system memberlist &>/dev/null || (
  echo "# Creating metallb secret"
  kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
  echo ""
)

%{~ if local.certmanager_installed }
echo "# Install certmanager with lets encrypt"
helm upgrade --install --namespace cert-manager --create-namespace cert-manager jetstack/cert-manager --values cert-manager.yaml
while [ "$(kubectl -n cert-manager get po | awk '/cert-manager-webhook/ { print $3}')" != "Running" ]; do
  sleep 1
done
kubectl apply -n cert-manager -f letsencrypt.yaml | grep -v unchanged
%{~ endif }

# install ingress controller
%{~ if var.ingress_controller == "traefik1" }
echo "# Install traefik v1 on ${local.ingress_private_ip} / ${local.ingress_public_ip}"
helm upgrade --install --create-namespace --namespace traefik traefik stable/traefik --values traefik1.yaml
echo ""
%{~ endif }
%{~ if var.ingress_controller == "traefik2" }
echo "# Install traefik v2 on ${local.ingress_private_ip} / ${local.ingress_public_ip}"
helm upgrade --install --create-namespace --namespace traefik traefik traefik/traefik --values traefik2.yaml
kubectl apply -n traefik -f traefik2_dashboard.yaml | grep -v unchanged
echo ""
%{~ endif }
%{~ if var.ingress_controller == "nginx" }
echo "# Install ingress-nginx on ${local.ingress_private_ip} / ${local.ingress_public_ip}"
helm upgrade --install --namespace nginx --create-namespace nginx ingress-nginx/ingress-nginx --values nginx.yaml
%{~ endif }
%{~ if var.ingress_controller == "nginxinc" }
echo "# Install nginx-stable on ${local.ingress_private_ip} / ${local.ingress_public_ip}"
echo "# THIS IS NOT WORKING YET"
helm upgrade --install --namespace nginx --create-namespace nginx nginx-stable/nginx-ingress --values nginxinc.yaml
%{~ endif }
%{~ if ! local.ingress_installed }
echo "# No ingress controller is installed.
%{~ endif }

# patch weave to allow for externalPolicy: Local
if [ "$(kubectl get daemonsets.apps -n kube-system weave-net -o yaml | grep NO_MASQ_LOCAL)" == "" ]; then
  echo '# Patching weavenet to allow externalPolicy=Local'
  kubectl patch daemonsets.apps -n kube-system weave-net --type='json' -p='[{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": {"name": "NO_MASQ_LOCAL", "value": "1" } }]' --dry-run=none
fi

# show README
echo ""
cat README
  EOT
}
