# 2021-06-02 

## Added
- now will add all worker nodes to ssh config
- mount condo on controlplane at /condo

# 2021-04-08 

## Added
- Add nginx and nginxinc as new ingress controllers (work in progress)
- Add certmanager if ingress is nginx
- traeif_acme_* is now acme_*

## Changed
- Switched to new nfs helm chart (no longer deprected)
- Tweaker docker json options based on official kubernetes docs
- Check mirrorlist.centos before installing
- Removed version from local provider

# 2021-03-17 

## Added
- Now will use volumes for controlplane and worker nodes
- Longhorn will be configured to backup to NFS

## Changed
- Weave now has DNSMASQ to allow for local policy (preseve user IP)
- enable_* renamed to *_enabled
- acme now by default uses non staging server

## Removed
- Removed unused helm provider

# 2021-02-24 

## Changed
- Only install if ingress_controller = "traefik" which is the default
- Setting traefik_access_logs = true to have traefik write to stdout the access logs (default is false)
- Install docker 20.10 by default
- Create a ~/.ssh/config.d/<cluster> file so you can use `ssh <cluster>-controlplane-0`
- Changed acme_variables to be prefixed with traefik to be consistent
- Both longhorn and monitoring are now enabled by default

# 2021-02-21 

This will want to change longhorn. There is no more longhorn-nfs implementation, all is done using RWXC containers. This is a big **breaking change**. Please backup your data from the NFS volume.

The change to longhorn could also cause issues trying to apply this to an existing cluster!

## Changed
- now using rancher_app_v2, this is a helm3 install not helm2 install. This works better with newer versions of rancher.
- longhorn now allows for RWX volumes
- traefik will always be exposed on https://traefik.<IP Address>.xip.io/
- default to Docker 19.03.14 on install
- will limit log files for docker to 10 files, each 100MB. This should prevent the system from running out of diskspace (disk pressure).
- will use newer monitoring chart (this is a more standard prometheus install)


# 2021-01-01 First Release

Initial setup of terraform to create kubernetes clusters on Radiant.
